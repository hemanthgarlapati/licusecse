/**
 * 
 */
package com.example.policy.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.policy.dto.TrendDto;
import com.example.policy.service.TrendServiceImpl;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class TestTrendController {
	
	@Mock
	TrendServiceImpl trendService;
	
	@InjectMocks
	TrendController trendController;

	
	@Test
	public void testGetTrendDetails() {
		TrendDto trendDto = new TrendDto(new Long(1), new Long(1), new Long(35));
		TrendDto trendDto1 = new TrendDto(new Long(1), new Long(1), new Long(65));
		List<TrendDto> dtoList = new ArrayList<TrendDto>();
		dtoList.add(trendDto);
		dtoList.add(trendDto1);
		when(trendService.getTrendDetails()).thenReturn(dtoList);
		ResponseEntity<List<TrendDto>> response=trendController.getTrendDetails();
		assertEquals(200, response.getStatusCodeValue());
		
	}
	
	/*
	 * @Test public void testgetCurrentTrendDetails() { TrendDto trendDto = new
	 * TrendDto(new Long(1), new Long(1), new Long(35)); TrendDto trendDto1 = new
	 * TrendDto(new Long(1), new Long(1), new Long(65)); List<TrendDto> dtoList =
	 * new ArrayList<TrendDto>(); dtoList.add(trendDto); dtoList.add(trendDto1);
	 * when(trendService.getCurrentTrendDetails()).thenReturn(dtoList);
	 * ResponseEntity<List<TrendDto>>
	 * response=trendController.getCurrentTrendDetails(); assertEquals(200,
	 * response.getStatusCodeValue());
	 * 
	 * }
	 */
}
