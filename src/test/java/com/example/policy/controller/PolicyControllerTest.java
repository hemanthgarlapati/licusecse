package com.example.policy.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.policy.dto.PolicyDto;
import com.example.policy.dto.PolicyFeaDto;
import com.example.policy.service.PolicyServiceImpl;
@SpringBootTest
public class PolicyControllerTest {
	@Mock
	PolicyServiceImpl policyServiceImpl;
	@InjectMocks
	PolicyController policyController;
	
	PolicyDto po=new PolicyDto();
	ArrayList<PolicyDto> polist=new ArrayList<PolicyDto>();
	PolicyFeaDto policefea=new PolicyFeaDto();

	
	@BeforeEach
	public void init() {
		po.setActiveStatus(true);
		po.setEnddate(LocalDate.now());
		polist.add(po);
		
		policefea.setActiveStatus(true);
		policefea.setEnddate(LocalDate.now());
	}
	@Test
	public void getAllPolices() {
		
		Mockito.when(policyServiceImpl.getAllPolicy()).thenReturn(polist);
		ResponseEntity<List<PolicyDto>> allPolicy = policyController.getAllPolicy();
		Assertions.assertEquals(1, allPolicy.getBody().size());
		
	}
	@Test
	public void getParticularPolicy() {
		Mockito.when(policyServiceImpl.getParticularPolicy(1L)).thenReturn(policefea);
		ResponseEntity<PolicyFeaDto> particularPolicy = policyController.getParticularPolicy(1L);
		Assertions.assertEquals(200,particularPolicy.getStatusCodeValue() );
	}

}
