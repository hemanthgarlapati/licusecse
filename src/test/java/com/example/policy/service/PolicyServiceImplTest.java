package com.example.policy.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.policy.dto.EmployeePolicyDto;
import com.example.policy.dto.PolicyDto;
import com.example.policy.dto.PolicyFeaDto;
import com.example.policy.entity.Employee;
import com.example.policy.entity.EmployeePolicy;
import com.example.policy.entity.Feature;
import com.example.policy.entity.Policy;
import com.example.policy.exception.EmployeeNotFoundException;
import com.example.policy.exception.PolicyNotFoundException;
import com.example.policy.repository.EmployeePolicyRepository;
import com.example.policy.repository.EmployeeRepository;
import com.example.policy.repository.PolicyRepository;
import com.example.policy.request.BuyPolicyRequest;

@SpringBootTest
public class PolicyServiceImplTest {
	@Mock
	EmployeePolicyRepository repository;

	@Mock
	PolicyRepository policyRepository;;

	@Mock
	EmployeeRepository employeeRepo;

	/*@InjectMocks
	PolicyServiceImpl service;
*/
	@InjectMocks
	PolicyServiceImpl policyServiceImpl;

	@BeforeEach
	public void init() {
		fea.setSailentFeatures("");
		fea.setPolicy(po);
		fea.setFeatureId(1l);
		fealist.add(fea);

		po.setActiveStatus(true);
		po.setEnddate(LocalDate.now());
		po.setEntryAge("12");
		po.setFeature(fealist);
		po.setMaximumMaturityAge("15 years");
		po.setMinimumPremium("1 lakh");
		po.setMinimumSumAssured("2 lakhs");
		po.setPolicyDesc("good desc");
		po.setPolicyId(1L);
		po.setPolicyTerm("5 years");
		po.setPolicyType("lic");
		po.setPurchasePrice("2 lakh");
		po.setStartdate(LocalDate.now());
		policy.add(po);
		
		e.setEmployeeId(1l);
		e.setEmpName("Test");
		e.setActiveStatus(true);

	}

	Feature fea = new Feature();
	ArrayList<Feature> fealist = new ArrayList<Feature>();
	ArrayList<Policy> policy = new ArrayList<Policy>();
	Policy po = new Policy();
	Employee e = new Employee();

	@Test
	public void testBuyPolicy() {
		BuyPolicyRequest request = new BuyPolicyRequest();

		request.setEmployeeId(22l);
		request.setPolicyid(1l);
		request.setPremiumAmount(1000l);

		Assertions.assertThrows(EmployeeNotFoundException.class, () -> policyServiceImpl.buyPolicy(request));

	}

	
	/*
	 * @Test public void testBuyPolicy2() throws EmployeeNotFoundException {
	 * BuyPolicyRequest request = new BuyPolicyRequest(); request.setEmployeeId(1l);
	 * request.setPolicyid(1l);; EmployeePolicyDto dto = new EmployeePolicyDto();
	 * 
	 * 
	 * Mockito.when(employeeRepo.findById(Mockito.anyLong())).thenReturn(Optional.of
	 * (e));
	 * Mockito.when(policyRepository.findById(1L)).thenReturn(Optional.of(po));
	 * Mockito.when(repository.save(Mockito.any())).thenReturn(new
	 * EmployeePolicy()); dto = policyServiceImpl.buyPolicy(request);
	 * Assertions.assertNotNull(dto);
	 * 
	 * 
	 * }
	 */
	 
	  
	  @Test
	  public void testGetPolicies3() throws EmployeeNotFoundException {
		  
		  Mockito.when(employeeRepo.findById(Mockito.anyLong())).thenReturn(Optional.of(e));
		  
		  EmployeePolicy ep = new EmployeePolicy();
		  
		  List<EmployeePolicy> epl = new ArrayList<>();
		  epl.add(ep);
		  
		  Mockito.when(repository.findByEmployee(Mockito.any())).thenReturn(epl);
		 List<EmployeePolicyDto> dto = policyServiceImpl.getPolicies(1l);
		 Assertions.assertNotNull(dto);
		  
		  
	  }

	@Test
	public void getAllPolicy_Negative() {
		ArrayList<Policy> policy = new ArrayList<Policy>();
		try {
			Mockito.when(policyRepository.findAll()).thenReturn(policy);

		} catch (PolicyNotFoundException pex) {
			Assertions.assertEquals("Policy not found", pex);
		}
	}

	@Test
	public void getParticularPolicy() {

		Mockito.when(policyRepository.findById(1L)).thenReturn(Optional.of(po));
		PolicyFeaDto policefea = policyServiceImpl.getParticularPolicy(1L);
		Assertions.assertEquals(policefea.getActiveStatus(), true);

	}

	@Test
	public void getParticularPolicyNegative() {
		try {
			Mockito.when(policyRepository.findById(2L)).thenReturn(Optional.of(po));
		}

		catch (PolicyNotFoundException pex) {
			Assertions.assertEquals("Policy not found", pex);
		}

	}

	@Test
	public void testGetPolicyForEmployee() {

		Assertions.assertThrows(EmployeeNotFoundException.class, () -> policyServiceImpl.getPolicies(322l));

	}

	/*
	 * @Test public void testGetPolicyForEmployee2() throws
	 * EmployeeNotFoundException {
	 * 
	 * Long employeeId = 1l;
	 * 
	 * List<EmployeePolicyDto> list = service.getPolicies(employeeId);
	 * 
	 * Assertions.assertNotNull(list);
	 * 
	 * }
	 */

	@Test
	public void getAllPolicy() {

		Mockito.when(policyRepository.findAll()).thenReturn(policy);
		List<PolicyDto> allPolicy = policyServiceImpl.getAllPolicy();
		Assertions.assertEquals(allPolicy.size(), 1);

	}
}
