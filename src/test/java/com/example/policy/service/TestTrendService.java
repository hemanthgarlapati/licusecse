/**
 * 
 */
package com.example.policy.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import com.example.policy.dto.TrendDto;
import com.example.policy.repository.TrendRepository;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class TestTrendService {

	@Mock
	TrendRepository trendRepository;

	@InjectMocks
	TrendServiceImpl trendService;

	@BeforeAll
	public static void setup() {

	}

	@Test
	public void testGetTrendDetails() {
		TrendDto trendDto = new TrendDto(new Long(1), new Long(1), new Long(35));
		TrendDto trendDto1 = new TrendDto(new Long(1), new Long(1), new Long(65));
		List<TrendDto> dtoList = new ArrayList<TrendDto>();
		dtoList.add(trendDto);
		dtoList.add(trendDto1);
		when(trendRepository.getTrendDetails()).thenReturn(dtoList);
		assertEquals(dtoList, trendService.getTrendDetails());
	}

	/*
	 * @Test public void testGetCurrentTrendDetails() { TrendDto trendDto = new
	 * TrendDto(new Long(1), new Long(1), new Long(75)); TrendDto trendDto1 = new
	 * TrendDto(new Long(2), new Long(2), new Long(25)); List<TrendDto> dtoList =
	 * new ArrayList<TrendDto>(); dtoList.add(trendDto); dtoList.add(trendDto1);
	 * when(trendRepository.getCurrentTrendDetails(PageRequest.of(0,
	 * 10))).thenReturn(dtoList); assertEquals(dtoList,
	 * trendService.getCurrentTrendDetails()); }
	 */

}
