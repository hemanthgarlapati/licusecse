/**
 * 
 */
package com.example.policy.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author hemanth.garlapati
 * 
 */
@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "employee_id")
	private Long employeeId;
	@Column(name = "emp_Name")
	private String empName;
	@Column(name = "active_status")
	private Boolean activeStatus;

	@OneToMany(mappedBy = "policy")
	private Set<EmployeePolicy> employeePolicies;

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Employee(Long employeeId, String empName, Boolean activeStatus) {
		super();
		this.employeeId = employeeId;
		this.empName = empName;
		this.activeStatus = activeStatus;
	}

	public Employee() {
		super();
	}

}
