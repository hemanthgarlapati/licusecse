
package com.example.policy.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "policy")
/**
 * @author rakeshr.ra
 * 
 * 
 * 
 */

public class Policy {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "policy_Id")
	Long policyId;
	@Column(name = "policy_Type")
	String policyType;

	public Policy() {
		super();
	}

	@Column(name = "policy_Desc")
	String policyDesc;
	@Column(name = "entryAge")
	String entryAge;
	@Column(name = "maximum_maturity_age")
	String maximumMaturityAge;
	@Column(name = "policy_term")
	String policyTerm;
	@Column(name = "minimum_premium")
	String minimumPremium;
	@Column(name = " purchase_price")
	String purchasePrice;
	@Column(name = "minimum_sum_Assured")
	String minimumSumAssured;
	@Column(name = "start_date")
	LocalDate startdate;
	@Column(name = "end_date")
	LocalDate enddate;
	@Column(name = "active_status")
	Boolean activeStatus;
	@OneToMany(mappedBy = "policy")
	List<Feature> feature;

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getPolicyDesc() {
		return policyDesc;
	}

	public void setPolicyDesc(String policyDesc) {
		this.policyDesc = policyDesc;
	}

	public String getEntryAge() {
		return entryAge;
	}

	public void setEntryAge(String entryAge) {
		this.entryAge = entryAge;
	}

	public String getMaximumMaturityAge() {
		return maximumMaturityAge;
	}

	public void setMaximumMaturityAge(String maximumMaturityAge) {
		this.maximumMaturityAge = maximumMaturityAge;
	}

	public String getPolicyTerm() {
		return policyTerm;
	}

	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm;
	}

	public String getMinimumPremium() {
		return minimumPremium;
	}

	public void setMinimumPremium(String minimumPremium) {
		this.minimumPremium = minimumPremium;
	}

	public String getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(String purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public String getMinimumSumAssured() {
		return minimumSumAssured;
	}

	public void setMinimumSumAssured(String minimumSumAssured) {
		this.minimumSumAssured = minimumSumAssured;
	}

	public List<Feature> getFeature() {
		return feature;
	}

	public void setFeature(List<Feature> feature) {
		this.feature = feature;
	}

	public LocalDate getStartdate() {
		return startdate;
	}

	public void setStartdate(LocalDate startdate) {
		this.startdate = startdate;
	}

	public LocalDate getEnddate() {
		return enddate;
	}

	public void setEnddate(LocalDate enddate) {
		this.enddate = enddate;
	}

	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

}
