package com.example.policy.entity;

import java.time.LocalDateTime;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class EmployeePolicy {
	
	 @EmbeddedId
	 EmployeePolicyPk employeePolicyPk;
	 
	    @ManyToOne
	    @MapsId("policy_id")
	    @JoinColumn(name = "policy_id")
	    Policy policy;
	 
	    @ManyToOne
	    @MapsId("employee_id")
	    @JoinColumn(name = "employee_id")
	    Employee employee;
	 
	    Long policyNumber;
	    
	    Long premiumAmount;
	    
	    LocalDateTime policyStartDate;
	    
	    LocalDateTime policyEndDate;
	    
	    Boolean activeStatus;

		public EmployeePolicyPk getEmployeePolicyPk() {
			return employeePolicyPk;
		}

		public void setEmployeePolicyPk(EmployeePolicyPk employeePolicyPk) {
			this.employeePolicyPk = employeePolicyPk;
		}

		public Policy getPolicy() {
			return policy;
		}

		public void setPolicy(Policy policy) {
			this.policy = policy;
		}

		public Employee getEmployee() {
			return employee;
		}

		public void setEmployee(Employee employee) {
			this.employee = employee;
		}

		public Long getPolicyNumber() {
			return policyNumber;
		}

		public void setPolicyNumber(Long policyNumber) {
			this.policyNumber = policyNumber;
		}

		public Long getPremiumAmount() {
			return premiumAmount;
		}

		public void setPremiumAmount(Long premiumAmount) {
			this.premiumAmount = premiumAmount;
		}

		public LocalDateTime getPolicyStartDate() {
			return policyStartDate;
		}

		public void setPolicyStartDate(LocalDateTime policyStartDate) {
			this.policyStartDate = policyStartDate;
		}

		public LocalDateTime getPolicyEndDate() {
			return policyEndDate;
		}

		public void setPolicyEndDate(LocalDateTime policyEndDate) {
			this.policyEndDate = policyEndDate;
		}

		public Boolean getActiveStatus() {
			return activeStatus;
		}

		public void setActiveStatus(Boolean activeStatus) {
			this.activeStatus = activeStatus;
		}


}
	