package com.example.policy.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Feature {
	public Long getFeatureId() {
		return featureId;
	}

	public void setFeatureId(Long featureId) {
		this.featureId = featureId;
	}

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long featureId;
	String sailentFeatures;
	@ManyToOne
	@JoinColumn
	Policy policy;

	public String getSailentFeatures() {
		return sailentFeatures;
	}

	public void setSailentFeatures(String sailentFeatures) {
		this.sailentFeatures = sailentFeatures;
	}
	

}
