package com.example.policy.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author bhagyashree.naray
 *
 */
@Embeddable
public class EmployeePolicyPk implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Column(name = "employee_id")
	Long employeeId;

	@Column(name = "policy_Id")
	Long policyId;

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public EmployeePolicyPk(Long employeeId, Long policyId) {
		super();
		this.employeeId = employeeId;
		this.policyId = policyId;
	}

	public EmployeePolicyPk() {
		super();

	}

}
