package com.example.policy.request;

public class BuyPolicyRequest {
	
	private Long employeeId;
	
	private Long policyid;
	
	private Long premiumAmount;
	
	String policyTerm;

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getPolicyid() {
		return policyid;
	}

	public void setPolicyid(Long policyid) {
		this.policyid = policyid;
	}

	public Long getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(Long premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public String getPolicyTerm() {
		return policyTerm;
	}

	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm;
	}
	
}
