/**
 * 
 */
package com.example.policy.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.policy.dto.TrendDto;
import com.example.policy.service.TrendServiceImpl;



/**
 * @author hemanth.garlapati
 * 
 */
@RestController
@RequestMapping(value = "trends")
public class TrendController {
	
	@Autowired
	TrendServiceImpl trendService;
	/**
	 * 
	 * @param nothing
	 * @return list of TrendDto
	 */
	
	@GetMapping(value="/overAll")
	public ResponseEntity<List<TrendDto>> getTrendDetails() {
		List<TrendDto> trendList=trendService.getTrendDetails();
		return new ResponseEntity<>(trendList, HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param nothing
	 * @return list of TrendDto
	 */
	
	@GetMapping(value="/current")
	public ResponseEntity<Object> getCurrentTrendDetails() {
		List<Map<Map<Long,Long>,Long>> trendList=trendService.getCurrentTrendDetails();
		return new ResponseEntity<>(trendList, HttpStatus.OK);
	}


}
