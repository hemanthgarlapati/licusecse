
package com.example.policy.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.policy.dto.EmployeePolicyDto;
import com.example.policy.dto.PolicyDto;
import com.example.policy.dto.PolicyFeaDto;
import com.example.policy.exception.EmployeeNotFoundException;
import com.example.policy.exception.PolicyNotFoundException;
import com.example.policy.request.BuyPolicyRequest;
import com.example.policy.service.PolicyService;


@RestController
public class PolicyController {
	
	private static final Logger logger = LoggerFactory.getLogger(PolicyController.class);
	@Autowired
	PolicyService policyService;

	/**Get all policies
	 * @return
	 */
	@GetMapping("/policies")
	public ResponseEntity<List<PolicyDto>> getAllPolicy() {
		logger.info("Getting all policies");
		return new ResponseEntity<>(policyService.getAllPolicy(),HttpStatus.OK);
		
	}
	/**Get policy
	 * @param policyid
	 * @return
	 */
	@GetMapping("/policies/policy/{policyid}")
	public ResponseEntity <PolicyFeaDto> getParticularPolicy(@PathVariable Long policyid) {
		logger.info("Getting policy based on policy id");
		return new ResponseEntity<>(policyService.getParticularPolicy(policyid),HttpStatus.OK);
		
		
	}
	
	/**API to buy policy
	 * @param request
	 * @return
	 * @throws EmployeeNotFoundException
	 * @throws PolicyNotFoundException
	 */


	public ResponseEntity<EmployeePolicyDto> buyPolicy(@RequestBody BuyPolicyRequest request) throws EmployeeNotFoundException {
		logger.info("inside buy policy");
		return new ResponseEntity<>(policyService.buyPolicy(request), HttpStatus.OK);
	}
	
	
	
	
	/**Get policy for employees
	 * @param employeeId
	 * @return
	 * @throws EmployeeNotFoundException
	 */
	@GetMapping("/employee/{employeeId}/policies")
	public ResponseEntity<List<EmployeePolicyDto>> getPolicies(@PathVariable Long employeeId) throws EmployeeNotFoundException  {
		return new ResponseEntity<>(policyService.getPolicies(employeeId), HttpStatus.OK);
	}
	
	

}
