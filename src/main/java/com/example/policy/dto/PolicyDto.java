package com.example.policy.dto;

import java.time.LocalDate;

public class PolicyDto {

	String policyType;

	String policyDesc;

	String entryAge;

	String maximumMaturityAge;

	String policyTerm;

	String minimumPremium;

	String purchasePrice;

	String minimumSumAssured;

	LocalDate startdate;

	LocalDate enddate;
	Boolean activeStatus;

	

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getPolicyDesc() {
		return policyDesc;
	}

	public void setPolicyDesc(String policyDesc) {
		this.policyDesc = policyDesc;
	}

	public String getEntryAge() {
		return entryAge;
	}

	public void setEntryAge(String entryAge) {
		this.entryAge = entryAge;
	}

	public String getMaximumMaturityAge() {
		return maximumMaturityAge;
	}

	public void setMaximumMaturityAge(String maximumMaturityAge) {
		this.maximumMaturityAge = maximumMaturityAge;
	}

	public String getPolicyTerm() {
		return policyTerm;
	}

	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm;
	}

	public String getMinimumPremium() {
		return minimumPremium;
	}

	public void setMinimumPremium(String minimumPremium) {
		this.minimumPremium = minimumPremium;
	}

	public String getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(String purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public String getMinimumSumAssured() {
		return minimumSumAssured;
	}

	public void setMinimumSumAssured(String minimumSumAssured) {
		this.minimumSumAssured = minimumSumAssured;
	}

	public LocalDate getStartdate() {
		return startdate;
	}

	public void setStartdate(LocalDate startdate) {
		this.startdate = startdate;
	}

	public LocalDate getEnddate() {
		return enddate;
	}

	public void setEnddate(LocalDate enddate) {
		this.enddate = enddate;
	}

	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	

}
