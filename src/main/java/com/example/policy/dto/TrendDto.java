/**
 * 
 */
package com.example.policy.dto;

/**
 * @author hemanth.garlapati
 * 
 */
public class TrendDto {

	private Long policyId;
	private Long count;
	private Long percentage;

	/**
	 * 
	 */
	public TrendDto() {
		super();
	}

	/**
	 * @param policyId
	 * @param count
	 * @param percentage
	 */
	public TrendDto(Long policyId, Long count, Long percentage) {
		super();
		this.policyId = policyId;
		this.count = count;
		this.percentage = percentage;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getPercentage() {
		return percentage;
	}

	public void setPercentage(Long percentage) {
		this.percentage = percentage;
	}

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

}
