
package com.example.policy.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.policy.dto.EmployeePolicyDto;
import com.example.policy.dto.PolicyDto;
import com.example.policy.dto.PolicyFeaDto;
import com.example.policy.exception.EmployeeNotFoundException;
import com.example.policy.request.BuyPolicyRequest;

@Service
public interface PolicyService {


	EmployeePolicyDto buyPolicy(BuyPolicyRequest request) throws EmployeeNotFoundException;




	public List<PolicyDto> getAllPolicy();
	PolicyFeaDto getParticularPolicy(Long policyid);
	List<EmployeePolicyDto> getPolicies(Long employeeId) throws EmployeeNotFoundException;

}
