/**
 * 
 */
package com.example.policy.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.example.policy.dto.TrendDto;

/**
 * @author hemanth.garlapati
 * 
 */
@Service
public interface TrendService {
	
	public List<TrendDto> getTrendDetails();
	
	public List<Map<Map<Long,Long>,Long>> getCurrentTrendDetails();

}
