
package com.example.policy.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.policy.dto.EmployeePolicyDto;
import com.example.policy.dto.PolicyDto;
import com.example.policy.dto.PolicyFeaDto;
import com.example.policy.entity.Employee;
import com.example.policy.entity.EmployeePolicy;
import com.example.policy.entity.EmployeePolicyPk;
import com.example.policy.entity.Feature;
import com.example.policy.entity.Policy;
import com.example.policy.exception.EmployeeNotFoundException;
import com.example.policy.exception.PolicyNotFoundException;
import com.example.policy.repository.EmployeePolicyRepository;
import com.example.policy.repository.EmployeeRepository;
import com.example.policy.repository.PolicyRepository;
import com.example.policy.request.BuyPolicyRequest;

@Service
public class PolicyServiceImpl implements PolicyService {

	private static final String POLICY_NOT_FOUND = "Policy not found";

	@Autowired
	EmployeePolicyRepository repository;

	@Autowired
	PolicyRepository policyRepository;

	@Autowired
	PolicyRepository policyRepo;

	@Autowired
	EmployeeRepository employeeRepo;

	@Override

	public EmployeePolicyDto buyPolicy(BuyPolicyRequest request) throws EmployeeNotFoundException {

		EmployeePolicyPk pk = new EmployeePolicyPk(request.getEmployeeId(), request.getPolicyid());

		EmployeePolicy policies = new EmployeePolicy();
		policies.setEmployeePolicyPk(pk);

		Optional<Employee> employeeOptional = employeeRepo.findById(request.getEmployeeId());

		if (!employeeOptional.isPresent()) {
			throw new EmployeeNotFoundException("Employee not found");
		}
		policies.setEmployee(employeeOptional.get());
		policies.setPolicyNumber(new Random().nextLong());
		policies.setActiveStatus(true);

		Optional<Policy> policyOptional = policyRepo.findById(request.getPolicyid());
		if (!policyOptional.isPresent()) {
			throw new PolicyNotFoundException(POLICY_NOT_FOUND);
		}

		policies.setPolicy(policyOptional.get());
		policies.setPremiumAmount(request.getPremiumAmount());
		policies.setPolicyStartDate(LocalDateTime.now());
		policies.setPolicyEndDate(LocalDateTime.now().plusYears(20));
		policies = repository.save(policies);

		EmployeePolicyDto dto = new EmployeePolicyDto();

		BeanUtils.copyProperties(policies, dto);

		return dto;
	}

	@Override
	public List<PolicyDto> getAllPolicy() {

		List<Policy> findAll = policyRepository.findAll();

		if (findAll.isEmpty()) {
			throw new PolicyNotFoundException(POLICY_NOT_FOUND);
		}

		List<PolicyDto> policy = new ArrayList<>();

		findAll.forEach(temp -> {
			PolicyDto policyDto = new PolicyDto();

			BeanUtils.copyProperties(temp, policyDto);
			policy.add(policyDto);
		});

		return policy;
	}

	@Override
	public PolicyFeaDto getParticularPolicy(Long policyid) {

		Optional<Policy> findById = policyRepository.findById(policyid);

		if (!findById.isPresent()) {
			throw new PolicyNotFoundException(POLICY_NOT_FOUND);
		}

		List<Feature> sailentfeatures = findById.get().getFeature();

		PolicyFeaDto policyFeaDto = new PolicyFeaDto();

		BeanUtils.copyProperties(findById.get(), policyFeaDto);
		List<String> sailent = new ArrayList<>();
		sailentfeatures.forEach(temps ->

		sailent.add(temps.getSailentFeatures()));
		policyFeaDto.setFea(sailent);
		return policyFeaDto;
	}

	@Override
	public List<EmployeePolicyDto> getPolicies(Long employeeId) throws EmployeeNotFoundException {

		Optional<Employee> employeeOptional = employeeRepo.findById(employeeId);

		if (!employeeOptional.isPresent()) {
			throw new EmployeeNotFoundException("Employee not found");
		}
		List<EmployeePolicy> policyEmpList = repository.findByEmployee(employeeOptional.get());

		return policyEmpList.stream().map(p -> {
			EmployeePolicyDto dto = new EmployeePolicyDto();
			BeanUtils.copyProperties(p, dto);
			return dto;
		}).collect(Collectors.toList());
	}

}
