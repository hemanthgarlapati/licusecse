/**
 * 
 */
package com.example.policy.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.policy.dto.TrendDto;
import com.example.policy.repository.TrendRepository;

/**
 * @author hemanth.garlapati
 * 
 */
@Service
public class TrendServiceImpl implements TrendService {
	
	@Autowired
	TrendRepository trendRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(TrendServiceImpl.class);
	
	/**
	 * 
	 * @param nothing
	 * @return list of TrendDto
	 */
	public List<TrendDto> getTrendDetails() {
		logger.info("getting details");
		return trendRepository.getTrendDetails();
		
	}
	
	/**
	 * 
	 * @param nothing
	 * @return list of TrendDto
	 */
	public List<Map<Map<Long,Long>,Long>> getCurrentTrendDetails() {
		logger.info("getting details for recent 10 applications");
		Pageable pageable = PageRequest.of(0, 10);
		return trendRepository.getCurrentTrendDetails(pageable);
		
		
	}

}
