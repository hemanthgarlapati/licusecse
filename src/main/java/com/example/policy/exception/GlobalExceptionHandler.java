package com.example.policy.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

	
	@ControllerAdvice
	public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
		
		@ExceptionHandler(EmployeeNotFoundException.class)
		public final ResponseEntity<Object> handleUserNotFoundException(EmployeeNotFoundException ex, WebRequest request) {
			List<String> message = new ArrayList<>();
			message.add(ex.getLocalizedMessage());
			ErrorResponse error = new ErrorResponse(4041l, message);
			return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
		}
		
		@ExceptionHandler(PolicyNotFoundException.class)
		public final ResponseEntity<Object> handlePolicyNotFoundException(PolicyNotFoundException ex, WebRequest request) {
			List<String> message = new ArrayList<>();
			message.add(ex.getLocalizedMessage());
			ErrorResponse error = new ErrorResponse(4041l, message);
			return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
		}
		
		 @ExceptionHandler(Exception.class)
		    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		        List<String> details = new ArrayList<>();
		        details.add(ex.getLocalizedMessage());
		        ErrorResponse error = new ErrorResponse(5001l, details);
		        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		 
		  @Override
		    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		        List<String> details = new ArrayList<>();
		        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
		            details.add(error.getDefaultMessage());
		        }
		        ErrorResponse error = new ErrorResponse(4001l, details);
		        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		    }
	}


