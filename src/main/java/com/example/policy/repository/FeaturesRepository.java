package com.example.policy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.policy.entity.Feature;
@Repository
public interface FeaturesRepository extends JpaRepository<Feature, Long>{

}
