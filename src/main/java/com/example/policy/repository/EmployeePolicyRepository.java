package com.example.policy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.policy.entity.Employee;
import com.example.policy.entity.EmployeePolicy;

@Repository
public interface EmployeePolicyRepository extends JpaRepository<EmployeePolicy, Long> {

	List<EmployeePolicy> findByEmployee(Employee employee);

}
