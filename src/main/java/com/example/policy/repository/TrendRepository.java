/**
 * 
 */
package com.example.policy.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.policy.dto.TrendDto;
import com.example.policy.entity.EmployeePolicy;

/**
 * @author hemanth.garlapati
 * 
 */

public interface TrendRepository extends JpaRepository<EmployeePolicy, Long> {

	@Query(value = "select new com.example.policy.dto.TrendDto(e.employeePolicyPk.policyId,count(e.employeePolicyPk.policyId) as policy_count,(Count(e.employeePolicyPk.policyId)* 100 / (SELECT count(e.employeePolicyPk.employeeId) "
			+ "FROM EmployeePolicy as e)) as percentage) from EmployeePolicy as e group by e.employeePolicyPk.policyId")
	public List<TrendDto> getTrendDetails();
	
	@Query(value = "select e.policy_id,count(e.policy_id) as policy_count,(Count(e.policy_id)* 100 / (SELECT count(e.employee_id) FROM employee_policies as e)) as percentage " + 
			" from (select * from employee_policies ORDER BY policy_start_date DESC LIMIT 10) as e group by e.policy_id ",nativeQuery = true)
	public List<Map<Map<Long,Long>,Long>> getCurrentTrendDetails(Pageable pageable);

}
