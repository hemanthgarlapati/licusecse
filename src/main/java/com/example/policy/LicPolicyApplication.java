package com.example.policy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LicPolicyApplication {

	public static void main(String[] args) {
		SpringApplication.run(LicPolicyApplication.class, args);
	}

}
